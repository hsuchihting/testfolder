class Animal {
  private name;
  public constructor(name: string) {
    this.name = name;
  }
}

class Cat extends Animal {
  constructor(name: string) {
    super(name);
    console.log(Animal.name);
  }
}
